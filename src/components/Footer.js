import React from 'react'
import styled from 'styled-components'

const FooterGroup = styled.div`
  text-align: center;
  margin-top: 64px;
  margin-bottom: 20px;
`

const Footer = () => (
  <FooterGroup>
    Made with
    <span role="img" aria-label="heart emoji">
      {' '}
      ❤️{' '}
    </span>
    somewhere in Ronalds Road
    <br />
    <br />
    <a href="mailto:david@saiwan.studio">Contact Us</a>
  </FooterGroup>
)

export default Footer
