import React from 'react'
import styled from 'styled-components'

const FormWrapper = styled.div`
  display: grid;
  grid-template-columns: 1;
  grid-template-rows: 2;
  justify-items: center;
  margin: 32px 0;
  @media (max-width: 640px) {
    margin: 0;
  }
`

const Button = styled.button`
  font-weight: 600;
  color: #fff;
  text-transform: uppercase;
  background: #3e30e0;
  padding: 18px 32px;
  border-radius: 32px;
  text-decoration: none;
  box-shadow: 0px 20px 40px rgba(0, 0, 0, 0.25);
  transition: transform 0.05s linear;
  outline: none;
  border: none;
  &::-moz-focus-inner {
    border: 0;
  }
  &:hover {
    cursor: pointer;
    transform: scale(1.05);
    box-shadow: 0 30px 60px rgba(0, 0, 0, 0.4);
  }
  &:focus {
    outline: 2px dotted #3e30e0;
  }
`

const CloseButton = styled.button`
  font-weight: 600;
  color: #3e30e0;
  text-transform: uppercase;
  background: #fff;
  padding: 18px 32px;
  border-radius: 32px;
  box-shadow: 0px 20px 40px rgba(0, 0, 0, 0.25);
  transition: transform 0.05s linear;
  outline: none;
  border: none;
  margin-right: 16px;
  &:hover {
    cursor: pointer;
    transform: scale(1.05);
    box-shadow: 0 30px 60px rgba(0, 0, 0, 0.4);
  }
  &::-moz-focus-inner {
    border: 0;
  }
  &:focus {
    outline: 2px dotted #3e30e0;
  }
`

const FormLabel = styled.label`
  display: block;
  font-size: 0.9em;
  padding: 0 0 4px 4px;
  font-weight: 600;
  text-transform: uppercase;
  span {
    padding-left: 5px;
    font-size: 10px;
  }
`

const FormInput = styled.input`
  justify-self: center;
  border: 2px solid #07033d;
  width: 500px;
  border-radius: 12px;
  padding: 12px;
  outline: none;
  font-size: 18px;
  margin: 5px 0 15px 0;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen,
    Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
  &:focus {
    outline: none;
    border-color: #3e30e0;
  }
  @media (max-width: 640px) {
    width: 340px;
  }
`

class SubmitForm extends React.Component {
  state = {
    active: false,
  }

  render() {
    return (
      <FormWrapper>
        <Button
          id="open-form"
          onClick={() => this.setState({ active: true })}
          style={{ display: this.state.active ? 'none' : 'block' }}
        >
          Submit yours
        </Button>
        <form
          action="https://getform.io/f/38d505c6-2016-4cbd-add2-5ad8184187e2"
          method="POST"
          id="form"
          style={{ display: this.state.active ? 'block' : 'none' }}
        >
          <div className="business-name">
            <FormLabel htmlFor="submission">
              Business name<span>*</span>
            </FormLabel>
            <FormInput
              id="submission"
              name="submission"
              type="text"
              placeholder="ie. Zia Lucia 🍕‍"
              required
              aria-required="true"
              minLength="2"
              maxLength="240"
            />
          </div>
          <div className="business-info">
            <FormLabel htmlFor="business-info">
              Business info<span>*</span>
            </FormLabel>

            <FormInput
              id="info"
              name="info"
              type="text"
              required
              placeholder="ie. Website, Phone, etc"
              aria-required="true"
              minLength="2"
              maxLength="240"
            />
          </div>
          <div className="submitter-name">
            <FormLabel htmlFor="your-name">
              Your name<span>optional</span>
            </FormLabel>

            <FormInput
              id="your-name"
              name="your-name"
              type="text"
              aria-required="true"
              minLength="5"
              maxLength="240"
            />
          </div>
          <CloseButton onClick={() => this.setState({ active: false })}>
            Cancel
          </CloseButton>
          <Button type="submit">Submit</Button>
        </form>
      </FormWrapper>
    )
  }
}

export default SubmitForm
