import React from 'react'
import { Link } from 'gatsby'
import styled from 'styled-components'

const HeaderGroup = styled.div`
  display: grid;
  grid-template-columns: center;
  justify-items: center;
  margin-top: 64px;
`

const HeaderTitle = styled.h1`
  font-weight: 900;
  font-size: 72px;
  text-align: center;
  margin: 24px 0;
  a {
    text-decoration: none;
  }

  @media (max-width: 640px) {
    font-size: 50px;
  }
`

const HeaderSubTitle = styled.p`
  font-size: 24px;
  text-align: center;
  margin-top: 0;
  padding: 0 18px;
  max-width: 600px;
  @media (max-width: 640px) {
    font-size: 18px;
  }
`

const Header = () => (
  <HeaderGroup>
    <svg
      width="150"
      height="157"
      viewBox="0 0 100 125"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
    >
      <path
        d="M19,22H17V20h2Zm2-4H19v2h2Zm-2,6h2V22H19Zm4-2V20H21v2ZM60,81h2V79H60Zm4-4H62v2h2Zm-2,6h2V81H62Zm4-2V79H64v2ZM80,19H78v2h2ZM75,87H73v2h2ZM50,91H18v2H82V91H50ZM29,87v2H71V87H29Zm-2,0H25v2h2Zm-4.46-8.9,1.34-1.48a39.11,39.11,0,0,1-11.2-40.35l-1.91-.58A41.08,41.08,0,0,0,22.54,78.1Zm54.92,0A41.12,41.12,0,0,0,91,47.65a40.64,40.64,0,0,0-8.18-24.57l-1.6,1.2A38.67,38.67,0,0,1,89,47.65a39.12,39.12,0,0,1-12.88,29ZM50,9a39.07,39.07,0,0,1,24.3,8.5l1.25-1.56A41,41,0,0,0,50,7a40.55,40.55,0,0,0-15.59,3.08l.76,1.85A38.42,38.42,0,0,1,50,9ZM36.71,70.22a4,4,0,1,1,5.22,2.17A4,4,0,0,1,36.71,70.22Zm1.85-.76a2,2,0,1,0,1.08-2.62A2,2,0,0,0,38.56,69.46Zm-7.94,4.36L26.8,64.59l2.15-.9,0-13,10.78-4.46L35.93,37l26.79-11.1.77,1.85-1.85.76-6.47,2.68L38.54,38.07l3.07,7.39L46.2,56.55,47,58.4l-1.84.76L29.41,65.67l2.3,5.54L34.56,70a5.9,5.9,0,0,0,.3,1,5.81,5.81,0,0,0,.47.89Zm.33-11,13.4-5.55-3.83-9.23L31,52Zm28.4-.93a5.81,5.81,0,0,1-.47-.89,7.07,7.07,0,0,1-.3-1l-13.1,5.43a5.71,5.71,0,0,1,.47.88,7.07,7.07,0,0,1,.3,1ZM71.91,48.06l-.77-1.84L48.05,55.78l.76,1.85,21.25-8.8,2.3,5.54L69.5,55.56a6.26,6.26,0,0,1,.47.89,6.54,6.54,0,0,1,.3.95L75,55.46ZM60.73,60.27A4,4,0,1,1,66,62.44,4,4,0,0,1,60.73,60.27Zm1.85-.76a2,2,0,1,0,1.08-2.62A2,2,0,0,0,62.58,59.51Zm-7-27.4L56.32,34l13.86-5.74-.77-1.84Zm1.53,3.69.76,1.85,13.86-5.74L71,30.06Zm1.53,3.7.76,1.85,13.86-5.74-.76-1.85Zm16.15-.2L74,37.45,60.15,43.19,60.91,45ZM61.68,46.89l.76,1.85L76.3,43l-.76-1.85Z"
        fill="#3E30E0"
      />
    </svg>
    <HeaderTitle>
      <Link to="/">London Deliveries</Link>
    </HeaderTitle>
    <HeaderSubTitle>
      During this quarantine let's all put together a list of places that
      deliver goods in and around London{' '}
      <span role="img" aria-label="prayer hands emoji">
        🙏
      </span>
    </HeaderSubTitle>
  </HeaderGroup>
)

export default Header
