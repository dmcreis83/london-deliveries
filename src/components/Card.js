import React from 'react'
import styled from 'styled-components'
import TwitterButton from './TwitterButton'

import location from '../images/location.svg'

const CardWrapper = styled.div`
  width: 500px;
  border-radius: 12px;
  background-color: #fafafa;
  padding: 18px 32px;
  margin: 24px 0;
  box-shadow: 0 20px 40px rgba(0, 0, 0, 0.25);
  display: grid;
  grid-template-rows: 1;
  transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);
  position: relative;
  @media (max-width: 640px) {
    width: 300px;
  }
`

const CardTitle = styled.h2`
  font-weight: 800;
  font-size: 1.5rem;
  line-height: 1;
  margin-bottom: 5px;
  a {
    text-decoration: none;
  }
`

const CardAddress = styled.p`
  line-height: 1.4;
  padding-left: 30px;
  position: relative;
  &:before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 20px;
    height: 20px;
    background-image: url(${location});
    background-position: center;
    background-size: cover;
  }
`
const CardDescription = styled.p`
  line-height: 1.4;
  margin-top: 8px;
`

const CardUrl = styled.p`
  line-height: 1.4;
  margin-top: 8px;
  text-decoration: underline;
`

const CardSubmitter = styled.p`
  position: absolute;
  right: 25px;
  bottom: 15px;
  color: #c8c8c8;
  font-size: 0.7rem;
  @media (max-width: 640px) {
    position: relative;
    right: auto;
    bottom: auto;
  }
`

const Card = ({ title, address, description, url, submitter }) => (
  <CardWrapper>
    <CardTitle>
      <a href={url} target="_blank" rel="noopener noreferrer">
        {title}
      </a>
    </CardTitle>
    <CardAddress>{address}</CardAddress>
    <CardDescription>{description}</CardDescription>
    <CardUrl>
      <a href={url} target="_blank" rel="noopener noreferrer">
        {url}
      </a>
    </CardUrl>
    <CardSubmitter>Submitted by {submitter}</CardSubmitter>
    <TwitterButton data={title} />
  </CardWrapper>
)

export default Card
