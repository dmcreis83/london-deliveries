import React from 'react'
import { StaticQuery, graphql } from 'gatsby'
import Helmet from 'react-helmet'
import Header from '../components/Header'
import Footer from '../components/Footer'

import './index.css'

export default () => (
  <StaticQuery
    query={graphql`
      query ContentQuery404 {
        allContentfulAsset {
          edges {
            node {
              file {
                url
                fileName
                contentType
              }
            }
          }
        }
      }
    `}
    render={data => (
      <div>
        <Helmet>
          <title>404 | London Deliveries</title>
          <meta name="theme-color" content="#fff" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />
          <meta
            name="Description"
            content="During this quarantine let's all put together a list of places that deliver goods in and around London"
          />
          <meta
            name="Keywords"
            content="deliveries, highbury, islington, covid, quarantine, london"
          />
          <meta
            name="Image"
            content={data.allContentfulAsset.edges[0].node.file.url}
          />
          <link
            rel="shortcut icon"
            href={data.allContentfulAsset.edges[0].node.file.url}
          />
          <html lang="en" />
        </Helmet>
        <div>
          <Header />
          <div className="center">
            <h1>NOT FOUND</h1>
            <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
            <a href="/">Go Back Home</a>
          </div>
          <Footer />
        </div>
      </div>
    )}
  />
)
