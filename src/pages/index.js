import React from 'react'
import { StaticQuery, graphql, Link } from 'gatsby'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import Header from '../components/Header'
import Footer from '../components/Footer'
import SubmitForm from '../components/SubmitForm'
import CardList from '../components/CardList'
import BackToTopButton from '../components/BackToTopButton'
import ScrollToTop from 'react-scroll-up'

import favIcon from '../images/favicon.png'

import './index.css'

const SkipLink = styled.a`
  position: absolute;
  top: -999vw;
  background: #3e30e0;
  color: #fff;
  padding: 0.75em 1.5em;
  border-radius: 4px;
  text-decoration: none;
`
export default () => (
  <StaticQuery
    query={graphql`
      query SiteMetadata {
        site {
          siteMetadata {
            description
            siteUrl
            title
          }
        }
      }
    `}
    render={({
      site: {
        siteMetadata: { siteUrl, title, description },
      },
    }) => (
      <div>
        <Helmet>
          <html lang="en" />
          <title>{title}</title>
          <link rel="canonical" href={`${siteUrl}`} />
          <meta name="docsearch:version" content="2.0" />
          <meta
            name="viewport"
            content="width=device-width,initial-scale=1,shrink-to-fit=no,viewport-fit=cover"
          />
          <meta name="Description" content={description} />
          <meta
            name="Keywords"
            content="deliveries, highbury, islington, covid, quarantine, london"
          />
          <meta name="Image" content={favIcon} />
          <link rel="shortcut icon" href={favIcon} />

          <meta property="og:url" content={siteUrl} />
          <meta property="og:type" content="website" />
          <meta property="og:locale" content="en" />
          <meta property="og:site_name" content={title} />
          <meta property="og:image" content={`${siteUrl}${favIcon}`} />
          <meta property="og:description" content={description} />
          <meta property="og:image:width" content="500" />
          <meta property="og:image:height" content="500" />
        </Helmet>
        <div>
          <SkipLink href="#card-list">Skip to main content</SkipLink>
          <Header />
          <SubmitForm />
          <div id="all-list" className="filter">
            <nav>
              <ul>
                <li className="selected">
                  <Link to="/#all-list">All</Link>
                </li>
                <li>
                  <Link to="/beer-cider#beer-list">Beer &amp; Cider</Link>
                </li>
                <li>
                  <Link to="/groceries#groceries-list">Groceries</Link>
                </li>
                <li>
                  <Link to="/meal-kits#meal-kits-list">Meal Kits</Link>
                </li>
                <li>
                  <Link to="/wine#wine-list">Wine</Link>
                </li>
              </ul>
            </nav>
          </div>
          <CardList />
          <ScrollToTop showUnder={160}>
            <BackToTopButton />
          </ScrollToTop>
          <Footer />
        </div>
      </div>
    )}
  />
)
