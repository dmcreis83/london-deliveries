import React from 'react'
import { StaticQuery, graphql, Link } from 'gatsby'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import Header from '../components/Header'
import Footer from '../components/Footer'
import SubmitForm from '../components/SubmitForm'
import CardListWine from '../components/CardListWine'
import BackToTopButton from '../components/BackToTopButton'
import ScrollToTop from 'react-scroll-up'

import './index.css'

const SkipLink = styled.a`
  position: absolute;
  top: -999vw;
  background: #3e30e0;
  color: #fff;
  padding: 0.75em 1.5em;
  border-radius: 4px;
  text-decoration: none;
`
export default () => (
  <StaticQuery
    query={graphql`
      query ContentQueryWine {
        allContentfulAsset {
          edges {
            node {
              file {
                url
                fileName
                contentType
              }
            }
          }
        }
      }
    `}
    render={data => (
      <div>
        <Helmet>
          <title>Wine | London Deliveries</title>
          <meta name="theme-color" content="#fff" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />
          <meta
            name="Description"
            content="During this quarantine let's all put together a list of places that deliver goods in and around London"
          />
          <meta
            name="Keywords"
            content="deliveries, highbury, islington, covid, quarantine, london"
          />
          <meta
            name="Image"
            content={data.allContentfulAsset.edges[0].node.file.url}
          />
          <link
            rel="shortcut icon"
            href={data.allContentfulAsset.edges[0].node.file.url}
          />
          <html lang="en" />
        </Helmet>
        <div>
          <SkipLink href="#card-list">Skip to main content</SkipLink>
          <Header />
          <SubmitForm />
          <div id="wine-list" className="filter">
            <nav>
              <ul>
                <li>
                  <Link to="/#all-list">All</Link>
                </li>
                <li>
                  <Link to="/beer-cider#beer-list">Beer &amp; Cider</Link>
                </li>
                <li>
                  <Link to="/groceries#groceries-list">Groceries</Link>
                </li>
                <li>
                  <Link to="/meal-kits#meal-kits-list">Meal Kits</Link>
                </li>
                <li className="selected">
                  <Link to="/wine#wine-list">Wine</Link>
                </li>
              </ul>
            </nav>
          </div>
          <CardListWine />
          <ScrollToTop showUnder={160}>
            <BackToTopButton />
          </ScrollToTop>
          <Footer />
        </div>
      </div>
    )}
  />
)
