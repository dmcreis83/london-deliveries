module.exports = {
  siteMetadata: {
    title: 'London Deliveries',
    siteUrl: `https://ldn.delivery`,
    description: `During this quarantine let's all put together a list of places that deliver goods in and around London`,
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-styled-components',
    {
      resolve: 'gatsby-source-contentful',
      options: {
        spaceId: 'gsp7fixjjeaa',
        accessToken: 'Iz-tNfglurCRQ_7ITMUOu9Q1WNwoRRikt1Jgihtc-RM',
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: 'UA-162632568-1',
      },
    },
    'gatsby-plugin-offline',
    'gatsby-plugin-netlify-cache',
  ],
}
